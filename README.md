# Leaderboard Animation #
[comment]: <> (A brief, descriptive title for the project.)


## Project Description ##
[comment]: <> (A high-level overview of the project's purpose and functionality.)
[comment]: <> (A brief explanation of the methods used for gathering data about websites, such as web scraping or APIs.)
There is a campaign with 10 streamers. The competition is very intense. Please make a scores
leaderboard for live updates and make the scores increase randomly. The scores of each player
will increase every second, and the leaderboard will automatically change the position of the
players according to the scores of each player.

## Live Application URL ##

The Application is deployed on **[https://your-application-URL](https://your-application-URL)**

## Development Application URL ##

The Application is deployed on **[https://your-application-URL](https://your-application-URL)**

## Local Application URL ##

The Application is deployed on **[http://localhost:3000](http://localhost:3000)**

---
## Table of Content ##
[comment]: <> (A quick navigation guide to the different sections of the README file.)

1. [ Prerequisites. ](#prerequisites)
2. [ Installation. ](#installation)
3. [ Usage tips. ](#usageTips)
4. [ Configuration. ](#configuration)
<!-- 4. [ Application Design. ](#appDesign) -->

---
<a name="prerequisites"></a>
## Prerequisites ##
[comment]: <> (List any system requirements or dependencies that users must have installed prior to using the project.)

For development, you will only need Node.js and a node global package installed in your environement.

Application React Version is 18.02.0 .

### Node
- #### Node installation on Windows

    Just go on [official Node.js website](https://nodejs.org/) and download the installer. 
  	Also, be sure to have `git` available in your PATH, `npm` might need it (You can find git [here](https://git-scm.com/)).

- #### Node installation on Ubuntu

	You can install nodejs and npm easily with apt install, just run the following commands.
```Javascript
    $ sudo apt install nodejs
    $ sudo apt install npm
```
	
- #### Other Operating Systems
  You can find more information about the installation on the [official Node.js website](https://nodejs.org/) and the [official NPM website](https://npmjs.org/).

	If the installation was successful, you should be able to run the following command.
```
    $ node --version
    V16.20.2

    $ npm --version
    V8.19.4
```
If you need to update `npm`, you can make it using `npm`! Cool right? After running the following command, just open again the command line and be happy.

    $ npm install npm -g

---
<a name="installation"></a>
## Installation ##
[comment]: <> (Step-by-step instructions on how to install the project and set up the environment.)
[comment]: <> (Include how to acquire API keys 'but obviously not the api keys themselves')

Clone the project into local
```
$ git clone https://gitlab.com/rpnikam70/leaderboardanimation.git
```
    
Install all the npm packages. Go into the project folder and type the following command to install all npm packages
```
$ cd leaderboardanimation
$ npm install
```

---
<a name="usageTips"></a>
## Usage tips ##
[comment]: <> (Provide clear instructions on how to use the project, including any relevant commands, input data formats, and examples.)
[comment]: <> (Explain the output data format and how to interpret the results.)
[comment]: <> (Outline what end-points or views have this project as a dependency.)

- #### Running the project ####
```
    $ npm start
```
The Application Runs on http://localhost:3000/

- #### Simple build for production ####
```
    $ npm run build
```
---
<a name="configuration"></a>
## Configuration ##
[comment]: <> (If applicable, explain any configuration options that can be customized by the user.)

_There is no any configuration options that was customized by the user._
